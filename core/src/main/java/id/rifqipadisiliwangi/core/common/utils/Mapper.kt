package id.rifqipadisiliwangi.core.common.utils

import id.rifqipadisiliwangi.core.data.remote.data.news.ResponseNewsItem
import id.rifqipadisiliwangi.core.domain.model.news.ItemNewsResponse

object Mapper {

    private fun ResponseNewsItem.Article.Source.toSourceMap() = ItemNewsResponse.ItemArticle.ItemSource(
        id = this.id, name = this.name
    )

    fun ResponseNewsItem.toNewsMapped() = ItemNewsResponse(
        articles = this.articles.toArticleList(),
        status = this.status,
        totalResults = this.totalResults
    )

    fun ResponseNewsItem.Article.toArticleMap() = ItemNewsResponse.ItemArticle(
        author = this.author,
        content = this.content,
        description = this.description,
        publishedAt = this.publishedAt,
        source = this.source?.toSourceMap(),
        title = this.title,
        url = this.url,
        urlToImage = this.urlToImage
    )
    fun Collection<ResponseNewsItem.Article>.toArticleList() = this.map { it.toArticleMap() }.toList()

}