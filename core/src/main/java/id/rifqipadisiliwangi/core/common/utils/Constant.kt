package id.rifqipadisiliwangi.core.common.utils

object Constant {

    val onboarding_one = "https://i.pinimg.com/564x/d0/83/e4/d083e4eebd49ea37cf77a07f96e5982a.jpg"
    val onboarding_two = "https://i.pinimg.com/564x/17/51/37/17513711867c77fa5aba9e9b30339ac3.jpg"
    val onboarding_three = "https://i.pinimg.com/564x/63/7a/40/637a4093ce0ce279537e41eea0a687ba.jpg"
    val app_name = "NewsApp"
    val IMAGE_FORMAT = "image/*"
    val IMAGE_PARSE = "image_parse"

}