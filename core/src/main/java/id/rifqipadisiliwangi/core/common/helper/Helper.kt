package id.rifqipadisiliwangi.core.common.helper

import android.app.Application
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import id.rifqipadisiliwangi.core.common.utils.Constant.app_name
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.Date
import java.util.Locale
import java.util.regex.Pattern

object Helper {


    val emailPattern = Pattern.compile("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")
    val passwordPattern = Pattern.compile(
        "^(?=.*[A-Z])" +
                "(?=.*[a-z])" +
                "[A-Za-z\\d@#\$%^&+=!]{8,}"
    )

    fun String.toSpannableHyperLink(
        locale: String,
        color :Int,
        registerInc: () -> Unit,
    ): SpannableString {
        val spannableString = SpannableStringBuilder(this)
        val registerText = if (locale =="in") "Sign Up" else "Sign Up"
        val startRegister = this.indexOf(registerText)
        val endRegister = startRegister + registerText.length


        val registerClickableSpan = object : ClickableSpan(){
            override fun onClick(widget: View) {
                registerInc.invoke()
            }
        }

        spannableString.setSpan(
            registerClickableSpan,
            startRegister,
            endRegister,
            0
        )
        spannableString.setSpan(
            ForegroundColorSpan(color),
            startRegister,
            endRegister,
            0
        )

        return SpannableString(spannableString)
    }



    fun String.toSpannableHyperLinkRegister(
        locale: String,
        color :Int,
        registerInc: () -> Unit,
    ): SpannableString {
        val spannableString = SpannableStringBuilder(this)
        val registerText = if (locale =="in") "Sign In" else "Sign In"
        val startRegister = this.indexOf(registerText)
        val endRegister = startRegister + registerText.length


        val registerClickableSpan = object : ClickableSpan(){
            override fun onClick(widget: View) {
                registerInc.invoke()
            }
        }

        spannableString.setSpan(
            registerClickableSpan,
            startRegister,
            endRegister,
            0
        )
        spannableString.setSpan(
            ForegroundColorSpan(color),
            startRegister,
            endRegister,
            0
        )

        return SpannableString(spannableString)
    }

    fun createFile(application: Application): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val mediaDir = application.externalMediaDirs.firstOrNull()?.let {
            File(it, app_name).apply { mkdirs() }
        }
        val outputDirectory = if (mediaDir != null && mediaDir.exists()) mediaDir else application.filesDir
        val imageFile = File(outputDirectory, "$timeStamp.jpg")
        compressAndSaveImage(imageFile)

        return imageFile
    }
    fun compressAndSaveImage(file: File) {
        val options = BitmapFactory.Options().apply {
            inSampleSize = 80
        }
        val bitmap = BitmapFactory.decodeFile(file.path, options)
        bitmap?.let {
            val fos = FileOutputStream(file)
            it.compress(Bitmap.CompressFormat.JPEG, 80, fos)
            fos.close()
        }
    }

    fun String.toFormattedDateTimeString(outputFormat: String): String {
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val dateTime = LocalDateTime.parse(this, formatter)
        val dateTimeUtc = dateTime.atZone(ZoneId.of("UTC"))
        return dateTimeUtc.format(DateTimeFormatter.ofPattern(outputFormat))
    }
}