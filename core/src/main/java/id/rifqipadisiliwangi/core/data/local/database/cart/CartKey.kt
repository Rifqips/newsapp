package id.rifqipadisiliwangi.core.data.local.database.cart

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cart_key")
data class CartKey(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val author: String,
    val content: String,
    val description: String,
    val publishedAt: String,
    val source: String,
    val title: String,
    val url: String,
    val urlToImage: String,
)
