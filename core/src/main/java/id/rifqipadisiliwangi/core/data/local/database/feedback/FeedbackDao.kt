package id.rifqipadisiliwangi.core.data.local.database.feedback

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface FeedbackDao {

    @Insert
    suspend fun insertFeedback(feedback: FeedbackKey)

    @Query("SELECT * FROM feedback_key")
    fun getAllFeedback(): Flow<List<FeedbackKey>>

    @Query("DELETE FROM feedback_key WHERE id = :id")
    fun deleteFeedbackId(id: String)

    @Query("DELETE FROM feedback_key")
    fun deleteAllFeedback()
}