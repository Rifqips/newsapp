package id.rifqipadisiliwangi.core.data.datasource

import id.rifqipadisiliwangi.core.data.remote.data.news.ResponseNewsItem
import id.rifqipadisiliwangi.core.data.remote.service.ApiEndpoint

interface NewsDataSource {
    suspend fun getListNews(domains:String? = null): ResponseNewsItem
    suspend fun getBitcoin(query:String? = null) : ResponseNewsItem
    suspend fun searchNews(query:String? = null) : ResponseNewsItem
}
class NewsDataSourceImpl(private val api : ApiEndpoint) : NewsDataSource{

    override suspend fun getListNews(domains: String?): ResponseNewsItem {
        return api.getListNews(domains)
    }

    override suspend fun getBitcoin(query: String?): ResponseNewsItem {
        return api.getBitcoin(query)

    }

    override suspend fun searchNews(query: String?): ResponseNewsItem {
        return api.searchNews(query)
    }
}
