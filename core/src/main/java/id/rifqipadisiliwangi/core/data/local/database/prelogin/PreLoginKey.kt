package id.rifqipadisiliwangi.core.data.local.database.prelogin

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pre_login_key")
data class PreLoginKey(
    @field:ColumnInfo("id")
    @field:PrimaryKey
    val id: Long? = 0,
    @field:ColumnInfo("email")
    var email: String? = "",
    @field:ColumnInfo("username")
    var username: String? = "",
    @field:ColumnInfo("password")
    var password: String? = ""
)
