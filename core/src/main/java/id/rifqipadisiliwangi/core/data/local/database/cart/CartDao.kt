package id.rifqipadisiliwangi.core.data.local.database.cart

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import id.rifqipadisiliwangi.core.data.local.database.feedback.FeedbackKey
import kotlinx.coroutines.flow.Flow


@Dao
interface CartDao {

    @Insert
    suspend fun insertCart(cart: CartKey)

    @Query("SELECT * FROM cart_key")
    fun getAllCart(): Flow<List<CartKey>>

    @Query("DELETE FROM cart_key WHERE id = :id")
    fun deleteCartId(id: String)

    @Query("SELECT * FROM cart_key WHERE id = :id")
    fun getCartById(id: String): Flow<List<CartKey>>
}