package id.rifqipadisiliwangi.core.data.local.database.feedback

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "feedback_key")
data class FeedbackKey(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val title: String,
    val message: String,
    val releaseDate: String,
)