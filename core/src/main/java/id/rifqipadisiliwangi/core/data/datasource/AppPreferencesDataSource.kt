package id.rifqipadisiliwangi.core.data.datasource

import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import id.rifqipadisiliwangi.core.common.helper.PreferenceDataStoreHelper
import kotlinx.coroutines.flow.Flow

interface AppPreferencesDataSource {
    suspend fun getUrl(): String

    suspend fun saveUrl(url: String)

    suspend fun getUsername(): String

    suspend fun saveUsername(username: String)
    fun getAppLayoutFlowWishList(): Flow<Boolean>
    suspend fun setAppLayoutPrefWishList(isGridLayout: Boolean)

}

class AppPreferencesDataSourceImpl(private val preferencesHelper : PreferenceDataStoreHelper) : AppPreferencesDataSource {
    override suspend fun getUrl(): String {
        return preferencesHelper.getFirstPreference(USER_URL,"")
    }

    override suspend fun saveUrl(url: String) {
        return preferencesHelper.putPreference(USER_URL, url)
    }

    override suspend fun getUsername(): String {
        return preferencesHelper.getFirstPreference(USERNAME,"")
    }

    override suspend fun saveUsername(username: String) {
        return preferencesHelper.putPreference(USERNAME, username)
    }

    override fun getAppLayoutFlowWishList(): Flow<Boolean> {
        return preferencesHelper.getPreference(PREF_GRID_LAYOUT_WISHLIST, false)
    }

    override suspend fun setAppLayoutPrefWishList(isGridLayout: Boolean) {
        return preferencesHelper.putPreference(PREF_GRID_LAYOUT_WISHLIST, isGridLayout)
    }

    companion object {
        val USER_URL = stringPreferencesKey("USER_URL")
        val USERNAME = stringPreferencesKey("USERNAME")
        val PREF_GRID_LAYOUT_WISHLIST = booleanPreferencesKey("PREF_GRID_LAYOUT_WISHLIST")
    }
}

