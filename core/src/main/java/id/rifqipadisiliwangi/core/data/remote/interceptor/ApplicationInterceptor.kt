package id.rifqipadisiliwangi.core.data.remote.interceptor

import okhttp3.Interceptor
import okhttp3.Response

class ApplicationInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
            .addHeader("Authorization", "apiKey 39afc4251bfa46ceba5f256a9a9cbe57")
            .build()
        return chain.proceed(request)
    }
}
