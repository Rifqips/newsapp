package id.rifqipadisiliwangi.core.data.local.database.prelogin

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PreLoginDao {

    @Query("SELECT * FROM pre_login_key WHERE email = :email AND password = :password")
    suspend fun authenticate(email:String, password: String): PreLoginKey?
    @Insert
    suspend fun register(user: PreLoginKey)

    @Query("SELECT * FROM pre_login_key WHERE username = :username")
    suspend fun getUser(username: String): PreLoginKey?
}