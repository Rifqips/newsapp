package id.rifqipadisiliwangi.core.data.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import id.rifqipadisiliwangi.core.data.local.database.cart.CartDao
import id.rifqipadisiliwangi.core.data.local.database.cart.CartKey
import id.rifqipadisiliwangi.core.data.local.database.feedback.FeedbackDao
import id.rifqipadisiliwangi.core.data.local.database.feedback.FeedbackKey
import id.rifqipadisiliwangi.core.data.local.database.prelogin.PreLoginDao
import id.rifqipadisiliwangi.core.data.local.database.prelogin.PreLoginKey

@Database(entities = [CartKey::class, FeedbackKey::class, PreLoginKey::class], version = 1 )
abstract class ApplicationDatabase: RoomDatabase() {
    abstract fun cartDao(): CartDao
    abstract fun feedbackDao(): FeedbackDao
    abstract fun preLoginDao(): PreLoginDao
    companion object{
        private var INSTANCE : ApplicationDatabase? = null
        fun getInstance(context: Context):ApplicationDatabase? {
            if (INSTANCE == null){
                synchronized(ApplicationDatabase::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        ApplicationDatabase::class.java,"application.db").build()
                }
            }
            return INSTANCE
        }
        fun destroyInstance(){
            INSTANCE = null
        }
    }
}