package id.rifqipadisiliwangi.core.data.remote.service

import id.rifqipadisiliwangi.core.data.remote.data.news.ResponseNewsItem
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiEndpoint {

    @GET("everything")
    suspend fun getListNews(
        @Query("domains") domains:String? = null,
    ): ResponseNewsItem

    @GET("everything")
    suspend fun getBitcoin(
        @Query("q") query:String? = null,
    ) : ResponseNewsItem

    @GET("everything")
    suspend fun searchNews(
        @Query("q") query:String? = null,
    ) : ResponseNewsItem
}