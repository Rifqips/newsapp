package id.rifqipadisiliwangi.core.domain.repository.database

import androidx.room.Insert
import androidx.room.Query
import id.rifqipadisiliwangi.core.data.local.database.cart.CartDao
import id.rifqipadisiliwangi.core.data.local.database.cart.CartKey
import id.rifqipadisiliwangi.core.data.local.database.feedback.FeedbackDao
import id.rifqipadisiliwangi.core.data.local.database.feedback.FeedbackKey
import id.rifqipadisiliwangi.core.data.local.database.prelogin.PreLoginDao
import id.rifqipadisiliwangi.core.data.local.database.prelogin.PreLoginKey
import kotlinx.coroutines.flow.Flow

interface AppRoomRepository {

    suspend fun register(user: PreLoginKey)
    suspend fun authenticate(email:String, password: String): PreLoginKey?
    suspend fun getUser(username: String): PreLoginKey?

    suspend fun insertFeedback(feedback: FeedbackKey)
    fun getAllFeedback(): kotlinx.coroutines.flow.Flow<List<FeedbackKey>>
    fun deleteFeedbackId(id: String)

    fun deleteAllFeedback()

    suspend fun insertCart(cart: CartKey)

    fun getAllCart(): Flow<List<CartKey>>

    fun deleteCartId(id: String)

    fun getCartById(id: String): Flow<List<CartKey>>


}

class AppRoomRepositoryImpl(
    private val preLoginDao: PreLoginDao,
    private val feedbackDao: FeedbackDao,
    private val cartDao: CartDao,

) : AppRoomRepository {
    override suspend fun register(user: PreLoginKey) {
        return preLoginDao.register(user)
    }

    override suspend fun authenticate(email: String, password: String): PreLoginKey? {
        return preLoginDao.authenticate(email, password)
    }

    override suspend fun getUser(username: String): PreLoginKey? {
        return preLoginDao.getUser(username)
    }

    override suspend fun insertFeedback(feedback: FeedbackKey) {
        return feedbackDao.insertFeedback(feedback)
    }

    override fun getAllFeedback(): kotlinx.coroutines.flow.Flow<List<FeedbackKey>> {
        return feedbackDao.getAllFeedback()
    }

    override fun deleteFeedbackId(id: String) {
        return feedbackDao.deleteFeedbackId(id)
    }

    override fun deleteAllFeedback() {
        return feedbackDao.deleteAllFeedback()
    }

    override suspend fun insertCart(cart: CartKey) {
        return cartDao.insertCart(cart)
    }

    override fun getAllCart(): Flow<List<CartKey>> {
        return cartDao.getAllCart()
    }

    override fun deleteCartId(id: String) {
        return cartDao.deleteCartId(id)
    }

    override fun getCartById(id: String): Flow<List<CartKey>> {
        return cartDao.getCartById(id)
    }

}
