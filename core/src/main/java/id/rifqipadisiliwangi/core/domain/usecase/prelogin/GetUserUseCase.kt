package id.rifqipadisiliwangi.core.domain.usecase.prelogin

import id.rifqipadisiliwangi.core.domain.repository.database.AppRoomRepository

class GetUserUseCase(private val appRoomRepository: AppRoomRepository) {
    suspend operator fun invoke(username : String) {
        appRoomRepository.getUser(username)
    }
}