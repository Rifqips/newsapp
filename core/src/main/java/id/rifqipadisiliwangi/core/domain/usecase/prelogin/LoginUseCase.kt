package id.rifqipadisiliwangi.core.domain.usecase.prelogin

import id.rifqipadisiliwangi.core.common.state.UiState
import id.rifqipadisiliwangi.core.domain.repository.firebase.FirebaseRepository
import kotlinx.coroutines.flow.Flow

class LoginUseCase(private val firebase: FirebaseRepository) {
    suspend operator fun invoke(email : String, password : String)  : Flow<UiState<Boolean>> {
        return firebase.loginUser(email, password)
    }
}