package id.rifqipadisiliwangi.core.domain.usecase.news

import id.rifqipadisiliwangi.core.domain.model.news.ItemNewsResponse
import id.rifqipadisiliwangi.core.domain.repository.news.NewsRepositoryImpl

class BitcoinUseCase(private val news : NewsRepositoryImpl) {

    suspend operator fun invoke(query : String) : ItemNewsResponse {
        return news.getBitcoin(query)
    }
}