package id.rifqipadisiliwangi.core.domain.repository.firebase

import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import id.rifqipadisiliwangi.core.common.state.UiState
import id.rifqipadisiliwangi.core.data.datasource.AppPreferencesDataSource
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await
import java.util.UUID

interface FirebaseRepository {
    suspend fun uploadImage(uri: Uri): Uri?
    fun debugSreenView(debug : String)
    fun logEvent(eventName : String, bundle : Bundle)
    fun logExeception(exception: Exception)

    suspend fun registerUser(email: String, password: String) : Boolean
    suspend fun loginUser(email: String, password: String) : Flow<UiState<Boolean>>
    fun checkEmailExistence(email: String, onComplete: (Boolean) -> Unit)
    fun getUserEmail(): String?

    fun getCurrentUser(): FirebaseUser?
    fun isLoggedIn(): Boolean
    fun logoutUser(callback: () -> Unit)
}
class FirebaseRepositoryImpl(
    private val preferencesSource : AppPreferencesDataSource,
    private val firebaseAnalytics: FirebaseAnalytics
) : FirebaseRepository{

    private val storageReference = FirebaseStorage.getInstance().reference
    private val auth = FirebaseAuth.getInstance()
    private val database = FirebaseDatabase.getInstance().reference.child("users")

    override suspend fun uploadImage(uri: Uri): Uri? {
        val ref = storageReference.child("images/${UUID.randomUUID()}")
        val downloadUrl: Uri? = null
        ref.putFile(uri).addOnSuccessListener { taskSnapshot ->
            ref.downloadUrl.addOnSuccessListener { uri ->
                runBlocking {
                    preferencesSource.saveUrl(uri.toString())
                }
            }.addOnFailureListener { _ ->
            }
        }.addOnFailureListener { e ->
            Log.e("FirebaseRepositoryLog", "Upload failed: $e")
        }.await()

        return downloadUrl
    }
    override fun debugSreenView(debug: String) {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, Bundle().apply { putString("screen_view",debug) })
    }

    override fun logEvent(eventName: String, bundle: Bundle) {
        firebaseAnalytics.logEvent(eventName, bundle)
    }
    override fun logExeception(exception: Exception) {
        FirebaseCrashlytics.getInstance().recordException(exception)
    }

    override suspend fun registerUser(email: String, password: String): Boolean {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful){
                    UiState.Success(task.isSuccessful)
                } else{
                    task.exception?.let { UiState.Error(it) }
                }
            }
        return true
    }

    override suspend fun loginUser(email: String, password: String): Flow<UiState<Boolean>> {
        return callbackFlow {
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful){
                        val uiState = UiState.Success(task.isSuccessful)
                        trySend(uiState)
                    } else {
                        val taskError = task.exception?.let { UiState.Error(it) }
                        trySend(UiState.Success(false))
                    }
                }
            awaitClose()
        }
    }

    override fun checkEmailExistence(email: String, onComplete: (Boolean) -> Unit) {
        database.child(email).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val isEmailExist = snapshot.exists()
                onComplete(isEmailExist)
            }

            override fun onCancelled(error: DatabaseError) {
                onComplete(false)
            }
        })
    }

    override fun getUserEmail(): String? {
        return auth.currentUser?.email
    }

    override fun getCurrentUser(): FirebaseUser? {
        return auth.currentUser
    }
    override fun isLoggedIn(): Boolean {
        return auth.currentUser != null
    }

    override fun logoutUser(callback: () -> Unit) {
        auth.signOut()
        callback()
    }

}