package id.rifqipadisiliwangi.core.domain.usecase.prelogin

import id.rifqipadisiliwangi.core.domain.repository.firebase.FirebaseRepository

class RegisterUseCase(private val firebase: FirebaseRepository) {
    suspend operator fun invoke(email : String, password : String) : Boolean {
        return firebase.registerUser(email, password)
    }
}