package id.rifqipadisiliwangi.core.domain.usecase.news

import id.rifqipadisiliwangi.core.domain.model.news.ItemNewsResponse
import id.rifqipadisiliwangi.core.domain.repository.news.NewsRepositoryImpl

class NewsUseCase(private val news : NewsRepositoryImpl) {

    suspend operator fun invoke(domain : String) : ItemNewsResponse {
        return news.getListNews(domain)
    }
}