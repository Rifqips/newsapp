package id.rifqipadisiliwangi.core.domain.usecase.news

import id.rifqipadisiliwangi.core.domain.model.news.ItemNewsResponse
import id.rifqipadisiliwangi.core.domain.repository.news.NewsRepositoryImpl

class SearchUseCase(private val news : NewsRepositoryImpl) {

    suspend operator fun invoke(query : String) : ItemNewsResponse {
        return news.searchNews(query)
    }
}