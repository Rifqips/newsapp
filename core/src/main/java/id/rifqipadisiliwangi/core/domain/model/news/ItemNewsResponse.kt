package id.rifqipadisiliwangi.core.domain.model.news

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class ItemNewsResponse(
    val articles: List<ItemArticle>,
    val status: String?,
    val totalResults: Int?
) : Parcelable {
    @Keep
    @Parcelize
    data class ItemArticle(
        val author: String?,
        val content: String?,
        val description: String?,
        val publishedAt: String?,
        val source: ItemSource?,
        val title: String?,
        val url: String?,
        val urlToImage: String?
    ) : Parcelable {
        @Keep
        @Parcelize
        data class ItemSource(
            val id: String?,
            val name: String?
        ) : Parcelable
    }
}
