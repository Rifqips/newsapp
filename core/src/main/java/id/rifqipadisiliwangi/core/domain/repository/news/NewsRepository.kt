package id.rifqipadisiliwangi.core.domain.repository.news

import id.rifqipadisiliwangi.core.common.utils.Mapper.toNewsMapped
import id.rifqipadisiliwangi.core.data.datasource.NewsDataSource
import id.rifqipadisiliwangi.core.domain.model.news.ItemNewsResponse

interface NewsRepository {

    suspend fun getListNews(domains:String? = null): ItemNewsResponse
    suspend fun getBitcoin(query:String? = null) : ItemNewsResponse
    suspend fun searchNews(query:String? = null) : ItemNewsResponse
}

class NewsRepositoryImpl(private val source : NewsDataSource) : NewsRepository{

    override suspend fun getListNews(domains: String?): ItemNewsResponse {
        return source.getListNews(domains).toNewsMapped()

    }

    override suspend fun getBitcoin(query: String?): ItemNewsResponse{
        return source.getBitcoin(query).toNewsMapped()
    }

    override suspend fun searchNews(query: String?): ItemNewsResponse {
        return source.searchNews(query).toNewsMapped()
    }
}
