package id.rifqipadisiliwangi.newsapp.presentation.viewmodel

import android.net.Uri
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import id.rifqipadisiliwangi.core.common.state.UiState
import id.rifqipadisiliwangi.core.common.utils.asMutableStateFlow
import id.rifqipadisiliwangi.core.data.datasource.AppPreferencesDataSource
import id.rifqipadisiliwangi.core.data.local.database.cart.CartKey
import id.rifqipadisiliwangi.core.data.local.database.feedback.FeedbackKey
import id.rifqipadisiliwangi.core.domain.model.news.ItemNewsResponse
import id.rifqipadisiliwangi.core.domain.repository.database.AppRoomRepository
import id.rifqipadisiliwangi.core.domain.repository.firebase.FirebaseRepository
import id.rifqipadisiliwangi.core.domain.usecase.news.BitcoinUseCase
import id.rifqipadisiliwangi.core.domain.usecase.news.NewsUseCase
import id.rifqipadisiliwangi.core.domain.usecase.news.SearchUseCase
import id.rifqipadisiliwangi.core.domain.usecase.prelogin.LoginUseCase
import id.rifqipadisiliwangi.core.domain.usecase.prelogin.RegisterUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class ApplicationVM(
    private val preferences: AppPreferencesDataSource,
    private val room: AppRoomRepository,
    private val loginUseCase: LoginUseCase,
    private val registerUseCase: RegisterUseCase,
    private val firebase : FirebaseRepository,
    private val newsUseCase: NewsUseCase,
    private val bitcoinUseCase: BitcoinUseCase,
    private val searchUseCase: SearchUseCase,
) : ViewModel() {

    private val _loading = MutableLiveData<Boolean>()
    val loading : LiveData<Boolean> = _loading

    private val _userEmail = MutableLiveData<String>()
    val userEmail : LiveData<String> = _userEmail

    private val _stateLogOut = MutableLiveData<Boolean>()
    val stateLoggOut : LiveData<Boolean> = _stateLogOut

    private val _loginUser : MutableStateFlow<UiState<Boolean>> = MutableStateFlow(UiState.Empty)
    val loginUser = _loginUser.asStateFlow()

    private val _listNews : MutableStateFlow<UiState<ItemNewsResponse>> = MutableStateFlow(UiState.Empty)
    val listNews = _listNews.asStateFlow()

    private val _isUrl  = MutableLiveData<String>()
    val isUrl : LiveData<String> = _isUrl

    private val _isUsername = MutableLiveData<String>()
    val isUsername : LiveData<String> = _isUsername

    private val _bitcoinNews : MutableStateFlow<UiState<ItemNewsResponse>> = MutableStateFlow(UiState.Empty)
    val bitcoinNews = _bitcoinNews.asStateFlow()

    private val _searchNews : MutableStateFlow<UiState<ItemNewsResponse>> = MutableStateFlow(UiState.Empty)
    val searchNews = _searchNews.asStateFlow()

    private val _userRegister : MutableStateFlow<UiState<Boolean>> = MutableStateFlow(UiState.Empty)
    val registerUser = _userRegister.asStateFlow()

    private val _feedbacklist = MutableLiveData<List<FeedbackKey>>()
    val feedbacklist: LiveData<List<FeedbackKey>> = _feedbacklist

    private val _cartlist = MutableLiveData<List<CartKey>>()
    val cartlist: LiveData<List<CartKey>> = _cartlist

    val appLayoutWishlistLiveData = preferences.getAppLayoutFlowWishList().asLiveData(Dispatchers.IO)

    init {
        getUrl()
        getUsername()
        checkLoginStatus()
        getAllFeedback()
        getListNews("techcrunch.com,thenextweb.com")
        bitcoinNews("bitcoin")
        logoutUser()
        getUserEmail()
    }
    fun getUsername(){
        viewModelScope.launch {
            val username = preferences.getUsername()
            _isUsername.postValue(username)
        }
    }
    fun getUrl(){
        viewModelScope.launch {
            val url = preferences.getUrl()
            _isUrl.postValue(url)
        }
    }
    fun addUsername(username : String){
        viewModelScope.launch {
            preferences.saveUsername(username)
        }
    }

    fun uploadImage(uri: Uri) {
        _loading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            firebase.uploadImage(uri)
            _loading.postValue(false)
        }
    }

    fun debugSreenView(debug : String){
        firebase.debugSreenView(debug)
    }

    fun logEvent(eventName : String, bundle : Bundle){
        firebase.logEvent(eventName, bundle)
    }

    fun login(email: String, password: String){
        viewModelScope.launch {
            loginUseCase.invoke(email, password).collect{ data ->
                _loginUser.update { data }
            }
        }
    }
    fun register(email: String, password: String){
        viewModelScope.launch {
            _userRegister.asMutableStateFlow {
                registerUseCase.invoke(email, password)
            }
        }
    }
    private fun checkLoginStatus() {
        viewModelScope.launch {
            if (firebase.isLoggedIn()) {
                _loading.postValue(true)
            } else {
                _loading.postValue(false)
            }
        }
    }
    private fun getListNews(domains:String){
        viewModelScope.launch {
            _listNews.asMutableStateFlow {
                newsUseCase.invoke(domains)
            }
        }
    }
    private fun bitcoinNews(query:String){
        viewModelScope.launch {
            _bitcoinNews.asMutableStateFlow {
                bitcoinUseCase.invoke(query)
            }
        }
    }
    fun searchNewsItem(query:String){
        viewModelScope.launch {
            _searchNews.asMutableStateFlow {
                searchUseCase.invoke(query)
            }
        }
    }

    fun insertFeedback(feedback: FeedbackKey){
        viewModelScope.launch {
            room.insertFeedback(feedback)
        }
    }
    fun getAllFeedback(){
        viewModelScope.launch(Dispatchers.IO) {
            room.getAllFeedback().collect{
                _feedbacklist.postValue(it)

            }
        }
    }
    fun deleteFeedbackId(id: String){
        viewModelScope.launch(Dispatchers.IO) {
            room.deleteFeedbackId(id)
        }
    }

    fun deleteAllFeedback(){
        viewModelScope.launch(Dispatchers.IO) {
            room.deleteAllFeedback()
        }
    }
    fun setUpLayoutWishlist(isGridLayout: Boolean){
        viewModelScope.launch {
            preferences.setAppLayoutPrefWishList(isGridLayout)
        }
    }


    fun insertCart(cart: CartKey){
        viewModelScope.launch(Dispatchers.IO) {
            room.insertCart(cart)
        }
    }

    fun getAllCart(){
        viewModelScope.launch(Dispatchers.IO) {
            room.getAllCart().collect{
                _cartlist.postValue(it)
            }
        }
    }

    fun deleteCartId(id: String){
        viewModelScope.launch(Dispatchers.IO) {
            room.deleteCartId(id)
        }
    }

    fun getCartById(id: String){
        viewModelScope.launch(Dispatchers.IO) {
            room.getCartById(id).collect{
                _cartlist.postValue(it)

            }
        }
    }
    fun logoutUser() {
        firebase.logoutUser {
            _stateLogOut.postValue(true)
        }
    }

    private fun getUserEmail(){
        viewModelScope.launch{
            _userEmail.value = firebase.getUserEmail()
        }
    }
}
