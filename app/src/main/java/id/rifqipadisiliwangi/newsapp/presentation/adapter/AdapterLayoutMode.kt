package id.rifqipadisiliwangi.newsapp.presentation.adapter

enum class AdapterLayoutMode {
    GRID,
    LINEAR
}