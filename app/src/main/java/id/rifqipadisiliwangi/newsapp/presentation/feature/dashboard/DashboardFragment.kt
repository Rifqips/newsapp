package id.rifqipadisiliwangi.newsapp.presentation.feature.dashboard

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.ViewPager2
import coil.load
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.core.common.state.onLoading
import id.rifqipadisiliwangi.core.common.state.onSuccess
import id.rifqipadisiliwangi.core.common.utils.Constant
import id.rifqipadisiliwangi.core.common.utils.launchAndCollectIn
import id.rifqipadisiliwangi.core.domain.model.news.ItemNewsResponse
import id.rifqipadisiliwangi.core.domain.model.onboarding.OnboardingDataItem
import id.rifqipadisiliwangi.newsapp.R
import id.rifqipadisiliwangi.newsapp.databinding.FragmentDashboardBinding
import id.rifqipadisiliwangi.newsapp.presentation.adapter.corousel.CorouselAdapter
import id.rifqipadisiliwangi.newsapp.presentation.adapter.newstoday.AdapterNewsTodayItem
import id.rifqipadisiliwangi.newsapp.presentation.adapter.recomendation.AdapterRecomendationItem
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.androidx.viewmodel.ext.android.viewModel

class DashboardFragment : BaseFragment<FragmentDashboardBinding, ApplicationVM>(FragmentDashboardBinding::inflate){

    override val viewModel: ApplicationVM by viewModel()

    private lateinit var vpCorousel: ViewPager2
    private lateinit var adapter: CorouselAdapter
    private lateinit var imageList: ArrayList<OnboardingDataItem>
    private lateinit var tabs: TabLayout

    private val adapterRecomendationItem: AdapterRecomendationItem by lazy {
        AdapterRecomendationItem{
            navigateToDetail(it)
        }
    }
    private val adapterNewsToday: AdapterNewsTodayItem by lazy {
        AdapterNewsTodayItem{
            navigateToDetailNews(it)
        }
    }
    override fun initView() {
        observeData()
        setUpvp()
        initListener()
        with(binding){
            rvNewsRecomendation.postDelayed({ rvNewsRecomendation.smoothScrollToPosition(0) }, 1000)
            rvNewsRecomendation.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = adapterRecomendationItem
            }
            rvNewsToday.postDelayed({ rvNewsToday.smoothScrollToPosition(0) }, 1000)
            rvNewsToday.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = adapterNewsToday
            }
        }

    }

    private fun initListener(){
        with(binding){
            tvShowArticleRecomendation.setOnClickListener {
                activity?.supportFragmentManager?.findFragmentById(R.id.container_navigation)?.findNavController()?.navigate(R.id.action_homeFragment_to_recomendationFragment)
            }
            tvShowArticleToday.setOnClickListener {
                activity?.supportFragmentManager?.findFragmentById(R.id.container_navigation)?.findNavController()?.navigate(R.id.action_homeFragment_to_newsTodayFragment)
            }
            containerHeaderLyt.root.setOnClickListener {
                activity?.supportFragmentManager?.findFragmentById(R.id.container_navigation)?.findNavController()?.navigate(R.id.action_homeFragment_to_searchFragment)
            }
        }
    }

    private fun observeData(){
        with(binding){
            viewModel.listNews.launchAndCollectIn(viewLifecycleOwner){ state->
                state.onLoading {
                    layoutStateMenu.root.isVisible = true
                    layoutStateMenu.pbLoading.isVisible = true
                    layoutStateMenu.tvError.isVisible = false
                }
                    .onSuccess {
                        adapterRecomendationItem.setData(it.articles.toList())
                        layoutStateMenu.root.isVisible = false
                        layoutStateMenu.pbLoading.isVisible = false
                        layoutStateMenu.tvError.isVisible = false
                    }
            }
            viewModel.bitcoinNews.launchAndCollectIn(viewLifecycleOwner){ state->
                state.onLoading {
                    layoutStateToday.root.isVisible = true
                    layoutStateToday.pbLoading.isVisible = true
                    layoutStateToday.tvError.isVisible = false
                }
                    .onSuccess {
                        adapterNewsToday.setData(it.articles.toList())
                        layoutStateToday.root.isVisible = false
                        layoutStateToday.pbLoading.isVisible = false
                        layoutStateToday.tvError.isVisible = false
                    }
            }
            viewModel.isUrl.observe(viewLifecycleOwner){ url ->
                ivUser.load(url)
            }
            viewModel.isUsername.observe(viewLifecycleOwner){ binding.tvUsername.text = it }
        }
    }

    private fun setUpvp() {
        vpCorousel = binding.vpCorousel
        tabs = binding.tabs

        imageList = arrayListOf(
            OnboardingDataItem(
                Constant.onboarding_one, resources.getString(R.string.title_onboarding_one),resources.getString(
                    R.string.description_onboarding_one)),
            OnboardingDataItem(
                Constant.onboarding_two,resources.getString(R.string.title_onboarding_two),resources.getString(
                    R.string.description_onboarding_two)),
            OnboardingDataItem(
                Constant.onboarding_three,resources.getString(R.string.title_onboarding_three),resources.getString(
                    R.string.description_onboarding_threee)),
        )
        adapter = CorouselAdapter(imageList)
        vpCorousel.adapter = adapter

        TabLayoutMediator(
            tabs,
            vpCorousel
        ) { tab, _ ->
            tab.customView = layoutInflater.inflate(R.layout.custom_tab_layout, null)
        }.attach()

    }

    override fun onDestroy() {
        super.onDestroy()
        binding.vpCorousel.unregisterOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {})
    }

    private fun navigateToDetail(item : ItemNewsResponse.ItemArticle){
        val bundle = Bundle()
        bundle.putParcelable("item-recomendation", item)
        activity?.supportFragmentManager?.findFragmentById(R.id.container_navigation)?.findNavController()?.navigate(R.id.action_homeFragment_to_detailFragment, bundle)
    }
    private fun navigateToDetailNews(item : ItemNewsResponse.ItemArticle){
        val bundle = Bundle()
        bundle.putParcelable("item-news", item)
        activity?.supportFragmentManager?.findFragmentById(R.id.container_navigation)?.findNavController()?.navigate(R.id.action_homeFragment_to_detailFragment, bundle)
    }
}
