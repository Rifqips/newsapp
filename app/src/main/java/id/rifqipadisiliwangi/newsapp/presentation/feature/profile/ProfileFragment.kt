package id.rifqipadisiliwangi.newsapp.presentation.feature.profile

import android.util.Log
import androidx.navigation.fragment.findNavController
import coil.load
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.newsapp.R
import id.rifqipadisiliwangi.newsapp.databinding.FragmentProfileBinding
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : BaseFragment<FragmentProfileBinding, ApplicationVM>(FragmentProfileBinding::inflate){

    override val viewModel: ApplicationVM by viewModel()

    override fun initView() {
        initListener()
        observeData()

    }

    private fun initListener(){
        binding.cvLogout.setOnClickListener {
            viewModel.stateLoggOut.observe(viewLifecycleOwner){state ->
                if (state){
                    activity?.supportFragmentManager?.findFragmentById(R.id.container_navigation)?.findNavController()?.navigate(R.id.action_homeFragment_to_loginFragment)
                }
            }
        }
    }

    private fun observeData(){
        viewModel.isUrl.observe(viewLifecycleOwner){ url ->
            binding.ivUser.load(url)
        }
        viewModel.isUsername.observe(viewLifecycleOwner){ binding.tvUsername.text = it }
        viewModel.userEmail.observe(viewLifecycleOwner) { email ->

            Log.d("user-email", "$${email}")

        }
    }

}