package id.rifqipadisiliwangi.newsapp.presentation.feature.home

import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import androidx.window.layout.WindowMetricsCalculator
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.newsapp.R
import id.rifqipadisiliwangi.newsapp.databinding.FragmentHomeBinding
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment :
    BaseFragment<FragmentHomeBinding, ApplicationVM>(FragmentHomeBinding::inflate){

    override val viewModel: ApplicationVM by viewModel()

    override fun initView() {
        setUpFragment()
    }

    private fun setUpFragment() {
        val navHostFragment =
            childFragmentManager
                .findFragmentById(R.id.container_bottom_navigation) as NavHostFragment
        val navController = navHostFragment.navController
        val metrics = context?.let {
            WindowMetricsCalculator.getOrCreate().computeCurrentWindowMetrics(it)
        }
        val withDp = metrics?.bounds?.width()?.div(resources.displayMetrics.density)
        with(binding){
            if (withDp != null) {
                when {
                    withDp < 600f -> {
                        val bottomNav = binding.bottomNavigation as BottomNavigationView
                        bottomNav.setupWithNavController(navController)
                        bottomNav.setOnItemSelectedListener {
                            when (it.itemId) {
                                R.id.menuHome -> containerBottomNavigation.findNavController()
                                    .navigate(R.id.dashboardFragment)

                                R.id.menuBookmark -> containerBottomNavigation.findNavController()
                                    .navigate(R.id.cartFragment)

                                R.id.menuFeedback -> containerBottomNavigation.findNavController()
                                    .navigate(R.id.feedbackFragment)

                                R.id.menuProfile -> containerBottomNavigation.findNavController()
                                    .navigate(R.id.profileFragment)
                            }
                            true
                        }
                    }
                    else -> {
                        val navDrawer = binding.bottomNavigation as NavigationView
                        navDrawer.setupWithNavController(navController)
                    }
                }
            }
        }
    }

}
