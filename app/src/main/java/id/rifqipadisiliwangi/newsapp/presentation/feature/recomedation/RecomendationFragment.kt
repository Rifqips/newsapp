package id.rifqipadisiliwangi.newsapp.presentation.feature.recomedation

import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.core.common.state.onLoading
import id.rifqipadisiliwangi.core.common.state.onSuccess
import id.rifqipadisiliwangi.core.common.utils.launchAndCollectIn
import id.rifqipadisiliwangi.newsapp.databinding.FragmentRecomendationBinding
import id.rifqipadisiliwangi.newsapp.presentation.adapter.recomendation.AdapterRecomendation
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.androidx.viewmodel.ext.android.viewModel

class RecomendationFragment : BaseFragment<FragmentRecomendationBinding, ApplicationVM>(
    FragmentRecomendationBinding::inflate){

    override val viewModel: ApplicationVM by viewModel()

    private val adapterRecomendationItem: AdapterRecomendation by lazy {
        AdapterRecomendation{}
    }

    override fun initView() {
        initListener()
        observeData()
        with(binding){
            rvRecomendationNews.postDelayed({ rvRecomendationNews.smoothScrollToPosition(0) }, 1000)
            rvRecomendationNews.apply {
                layoutManager = GridLayoutManager(context,2)
                adapter = adapterRecomendationItem
            }
        }
    }

    private fun initListener(){
        with(binding){
            ivBack.setOnClickListener {
                findNavController().navigateUp()
            }
        }
    }

    private fun observeData(){
        viewModel.listNews.launchAndCollectIn(viewLifecycleOwner){ state->
            state.onLoading {}
                .onSuccess {
                    adapterRecomendationItem.setData(it.articles.toList())
                }
        }
    }
}