package id.rifqipadisiliwangi.newsapp.presentation.feature.splash

import android.view.animation.AnimationUtils
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.newsapp.R
import id.rifqipadisiliwangi.newsapp.databinding.FragmentSplashBinding
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashFragment : BaseFragment<FragmentSplashBinding, ApplicationVM>(FragmentSplashBinding::inflate){

    override val viewModel: ApplicationVM by viewModel()

    override fun initView() {
        observeData()
        val animation = AnimationUtils.loadAnimation(requireActivity(), R.anim.fade_in)
        with(binding){
            tvTitleName.startAnimation(animation)
            tvTitleName.text = resources.getString(R.string.string_title_name)
        }
    }
    fun observeData() {
        with(viewModel){
            lifecycleScope.launch {
                delay(3500)
                loading.observe(viewLifecycleOwner){state ->
                    when(state){
                        true -> findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
                        false -> findNavController().navigate(R.id.action_splashFragment_to_onboardingFragment)
                    }
                }
            }

        }
    }
}
