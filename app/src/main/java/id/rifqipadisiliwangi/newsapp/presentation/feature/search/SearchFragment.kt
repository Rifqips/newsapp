package id.rifqipadisiliwangi.newsapp.presentation.feature.search

import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.core.common.state.onLoading
import id.rifqipadisiliwangi.core.common.state.onSuccess
import id.rifqipadisiliwangi.core.common.utils.launchAndCollectIn
import id.rifqipadisiliwangi.newsapp.databinding.FragmentSearchBinding
import id.rifqipadisiliwangi.newsapp.presentation.adapter.newstoday.AdapterNewsTodayItem
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.androidx.viewmodel.ext.android.viewModel


class SearchFragment : BaseFragment<FragmentSearchBinding, ApplicationVM>(FragmentSearchBinding::inflate){
    override val viewModel: ApplicationVM by viewModel()

    private val adapterNewsToday: AdapterNewsTodayItem by lazy {
        AdapterNewsTodayItem{}
    }
    override fun initView() {
        initListener()
        observeData()
        with(binding){
            rvSearchItem.postDelayed({ rvSearchItem.smoothScrollToPosition(0) }, 1000)
            rvSearchItem.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = adapterNewsToday
            }
        }
    }

    private fun initListener(){
        with(binding){
            ivBack.setOnClickListener {
                findNavController().navigateUp()
            }
            searchView
                .editText
                .setOnEditorActionListener { _, _, _ ->
                    searchBar.setText(searchView.text)
                    searchItem(searchView.text.toString())
                    true
                }
        }
    }

    private fun observeData(){
        viewModel.searchNews.launchAndCollectIn(viewLifecycleOwner){ state->
            state.onLoading {}
                .onSuccess {
                    adapterNewsToday.setData(it.articles.toList())
                }
        }
    }

    private fun searchItem(q : String){
        viewModel.searchNewsItem(q)
    }

}