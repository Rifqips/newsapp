package id.rifqipadisiliwangi.newsapp.presentation.adapter

interface ViewHolderBinder<T> {
    fun bind(item: T)
}