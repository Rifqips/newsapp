package id.rifqipadisiliwangi.newsapp.presentation.feature.cart

import android.app.AlertDialog
import android.content.DialogInterface
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.newsapp.databinding.FragmentCartBinding
import id.rifqipadisiliwangi.newsapp.presentation.adapter.cart.AdapterCartItem
import id.rifqipadisiliwangi.newsapp.presentation.adapter.recomendation.AdapterRecomendationItem
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.androidx.viewmodel.ext.android.viewModel

class CartFragment : BaseFragment<FragmentCartBinding, ApplicationVM>(FragmentCartBinding::inflate){

    override val viewModel: ApplicationVM by viewModel()

    private val adapterBookmarkItem: AdapterCartItem by lazy {
        AdapterCartItem{
            shownDialog(it.id.toString())
        }
    }

    override fun initView() {
        observeData()
        binding.rvBookmark.apply {
            layoutManager = GridLayoutManager(context,2)
            adapter = adapterBookmarkItem
        }
    }

    private fun observeData(){
        viewModel.getAllCart()
        viewModel.cartlist.observe(viewLifecycleOwner){
            adapterBookmarkItem.setData(it)
        }
    }
    private fun shownDialog(id : String) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Delete Bookmark")
        builder.setMessage("Are you sure you want delete Bookmark?")
        builder.setPositiveButton("Yes") { dialog: DialogInterface, which: Int ->
            viewModel.deleteCartId(id)
            dialog.dismiss()
        }
        builder.setNegativeButton("No") { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
        }
        builder.show()
    }
}