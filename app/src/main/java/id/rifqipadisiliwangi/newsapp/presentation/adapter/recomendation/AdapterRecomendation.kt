package id.rifqipadisiliwangi.newsapp.presentation.adapter.recomendation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import id.rifqipadisiliwangi.core.domain.model.news.ItemNewsResponse
import id.rifqipadisiliwangi.newsapp.databinding.ItemGridNewsBinding
import id.rifqipadisiliwangi.newsapp.presentation.adapter.ViewHolderBinder

class AdapterRecomendation(
    private val onClickListener: (ItemNewsResponse.ItemArticle) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val dataDiffer = AsyncListDiffer(
        this,
        object : DiffUtil.ItemCallback<ItemNewsResponse.ItemArticle>() {
            override fun areItemsTheSame(
                oldItem: ItemNewsResponse.ItemArticle,
                newItem: ItemNewsResponse.ItemArticle
            ): Boolean {
                return oldItem.author == newItem.author
            }

            override fun areContentsTheSame(
                oldItem: ItemNewsResponse.ItemArticle,
                newItem: ItemNewsResponse.ItemArticle
            ): Boolean {
                return oldItem.hashCode() == newItem.hashCode()
            }
        }
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            else -> {
                GridMenuItemViewHolder(
                    binding = ItemGridNewsBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ),
                    onClickListener
                )
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolderBinder<ItemNewsResponse.ItemArticle>).bind(dataDiffer.currentList[position])
    }

    override fun getItemCount(): Int = dataDiffer.currentList.size


    fun setData(data: List<ItemNewsResponse.ItemArticle>) {
        dataDiffer.submitList(data)
        notifyDataSetChanged()
    }

    fun refreshList() {
        notifyItemRangeChanged(0, dataDiffer.currentList.size)
    }


    class GridMenuItemViewHolder(
        private val binding: ItemGridNewsBinding,
        private val onClickListener: (ItemNewsResponse.ItemArticle) -> Unit
    ) : RecyclerView.ViewHolder(binding.root), ViewHolderBinder<ItemNewsResponse.ItemArticle> {
        override fun bind(item: ItemNewsResponse.ItemArticle) {
            with(binding) {
                ivProduct.load(item.urlToImage)
                tvTitleProduct.text = item.title
                tvDatetime.text = item.publishedAt
                tvContent.text = item.content
            }
            binding.root.setOnClickListener {
                onClickListener(item)
            }
        }
    }
}