package id.rifqipadisiliwangi.newsapp.presentation.feature.detail

import android.os.Bundle
import androidx.navigation.fragment.findNavController
import coil.load
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.core.data.local.database.cart.CartKey
import id.rifqipadisiliwangi.core.data.local.database.feedback.FeedbackKey
import id.rifqipadisiliwangi.core.domain.model.news.ItemNewsResponse
import id.rifqipadisiliwangi.newsapp.R
import id.rifqipadisiliwangi.newsapp.databinding.FragmentDetailBinding
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailFragment : BaseFragment<FragmentDetailBinding, ApplicationVM>(FragmentDetailBinding::inflate){

    override val viewModel: ApplicationVM by viewModel()

    override fun initView() {
        initListener()
        getBundle()
        getBundleNews()
    }

    private fun initListener(){
        with(binding){
            ivBack.setOnClickListener {
                findNavController().navigateUp()
            }
        }
    }
    private fun getBundle(){
        with(binding){
            arguments?.let { it->
                it.getParcelable<ItemNewsResponse.ItemArticle>("item-recomendation")?.let {
                    ivDetailNews.load(it.urlToImage)
                    tvTitle.text = it.title
                    tvContent.text = it.content
                    tvAuthor.text = it.author
                    tvDatetime.text= it.publishedAt
                    insertCart(
                        0,
                        it.author ?: "",
                        it.title?: "",
                        it.content?: "",
                        it.description?: "",
                        it.publishedAt?: "",
                        it.source.toString(),
                        it.url?: "",
                        it.urlToImage?: ""
                    )
                }
            }
        }
    }
    private fun getBundleNews(){
        with(binding){
            arguments?.let { it->
                it.getParcelable<ItemNewsResponse.ItemArticle>("item-news")?.let {
                    ivDetailNews.load(it.urlToImage)
                    tvTitle.text = it.title
                    tvContent.text = it.content
                    tvAuthor.text = it.author
                    tvDatetime.text= it.publishedAt
                    insertCart(
                        0,
                        it.author ?: "",
                        it.title?: "",
                        it.content?: "",
                        it.description?: "",
                        it.publishedAt?: "",
                        it.source.toString(),
                        it.url?: "",
                        it.urlToImage?: ""
                    )
                }
            }
        }
    }

    private fun insertCart(
        id : Int,
        author : String,
        title : String,
        content : String,
        description : String,
        publishedAt : String,
        source : String,
        url : String,
        urlToImage : String
    ){
        binding.ivBookmark.setOnClickListener {
            val cart = CartKey(
                id = id,
                author = author,
                title = title,
                content = content,
                description = description,
                publishedAt = publishedAt,
                source = source,
                url = url,
                urlToImage = urlToImage

            )
            viewModel.insertCart(cart)
        }

    }

    private fun getById(id : String){
        viewModel.getCartById(id)
        viewModel.cartlist.observe(viewLifecycleOwner){
            with(binding){
                when(it.size){
                    1 -> {
                        ivBookmark.load(R.drawable.ic_bookmark_filled)
                        ivBookmark.isClickable = false
                    }
                    else ->{
                        ivBookmark.load(R.drawable.ic_bookmark_outlined)
                        ivBookmark.isClickable = true
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val bun = Bundle()
        bun.apply { putString("detail-screen", "detail-screen") }
        analyticsFirebase("detail-screen", bun)
    }

    private fun analyticsFirebase(message : String, bundle : Bundle){
        viewModel.logEvent(message, bundle)
        viewModel.debugSreenView(message)
    }

}
