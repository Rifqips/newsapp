package id.rifqipadisiliwangi.newsapp.presentation.feature.feedback

import android.app.AlertDialog
import android.content.DialogInterface
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.GridLayoutManager
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.core.data.local.database.feedback.FeedbackKey
import id.rifqipadisiliwangi.newsapp.R
import id.rifqipadisiliwangi.newsapp.databinding.FragmentFeedbackBinding
import id.rifqipadisiliwangi.newsapp.presentation.adapter.AdapterLayoutMode
import id.rifqipadisiliwangi.newsapp.presentation.adapter.feedback.AdapterFeedbackItem
import id.rifqipadisiliwangi.newsapp.presentation.feature.bottomsheet.BottomSheetFragment
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.Calendar

class FeedbackFragment : BaseFragment<FragmentFeedbackBinding, ApplicationVM>(FragmentFeedbackBinding::inflate), BottomSheetFragment.OnFilterListener{

    private val bottomSheetFragment: BottomSheetFragment by lazy { BottomSheetFragment() }

    private val adapterFeedback: AdapterFeedbackItem by lazy {
        AdapterFeedbackItem(AdapterLayoutMode.LINEAR) {
            shownDialog(it.id.toString())
        }
    }

    override val viewModel: ApplicationVM by viewModel()

    override fun initView() {
        initListener()
        rvListWishlist()
        observeLayout()
        observeData()
        setupSwitchLayout()
        if(bottomSheetFragment.isAdded) { return }
    }

    private fun initListener(){
        with(binding){
            btnFeedback.setOnClickListener {
                bottomSheetFragment.setFilterListener(this@FeedbackFragment)
                bottomSheetFragment.show(childFragmentManager, resources.getString(R.string.string_bottomsheet))
            }
        }
    }

    private fun observeData(){
        viewModel.feedbacklist.observe(viewLifecycleOwner) { feedback ->
            with(binding){
                rvFeedback.apply {
                    layoutManager = GridLayoutManager(context, 2)
                    adapter = adapterFeedback
                    adapterFeedback.setData(feedback)
                }
            }
        }
    }
    private fun rvListWishlist() {
        val span = if (adapterFeedback.adapterLayoutMode == AdapterLayoutMode.LINEAR) 1 else 2
        binding.rvFeedback.apply {
            layoutManager = GridLayoutManager(requireActivity(), span)
            adapter = adapterFeedback
            adapterFeedback.refreshList()
        }
    }
    private fun observeLayout() {
        viewModel.appLayoutWishlistLiveData.observe(viewLifecycleOwner) { isGridLayout ->
            (binding.rvFeedback.layoutManager as GridLayoutManager).spanCount =
                if (isGridLayout) 2 else 1
            adapterFeedback.adapterLayoutMode =
                if (isGridLayout) AdapterLayoutMode.GRID else AdapterLayoutMode.LINEAR
            adapterFeedback.refreshList()
        }
    }
    private fun setupSwitchLayout() {
        binding.ivChangeLayout.setOnCheckedChangeListener  { _, isChecked ->
            viewModel.setUpLayoutWishlist(isChecked)
            when(isChecked){
                true -> binding.ivChangeLayout.chipIcon = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_grid) }
                else -> binding.ivChangeLayout.chipIcon = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_list_layout) }
            }
        }
    }

    override fun onFilterApplied(
        comment: String,
        rating: String
    ) {
        val calendar = Calendar.getInstance()
        val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH) + 1
        val year = calendar.get(Calendar.YEAR)
        val feedback = FeedbackKey(
            message = comment,
            title = rating,
            releaseDate = "$dayOfMonth/$month/$year",
        )
        viewModel.insertFeedback(feedback)
    }
    private fun shownDialog(id : String) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Delete Feedback")
        builder.setMessage("Are you sure you want delete feedback?")
        builder.setPositiveButton("Yes") { dialog: DialogInterface, which: Int ->
            viewModel.deleteFeedbackId(id)
            dialog.dismiss()
        }
        builder.setNegativeButton("No") { dialog: DialogInterface, which: Int ->
            dialog.dismiss()
        }
        builder.show()
    }

}