package id.rifqipadisiliwangi.newsapp.presentation.adapter.feedback

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import id.rifqipadisiliwangi.core.data.local.database.feedback.FeedbackKey
import id.rifqipadisiliwangi.newsapp.databinding.ItemGridFeedbackBinding
import id.rifqipadisiliwangi.newsapp.databinding.ItemListFeedbackBinding
import id.rifqipadisiliwangi.newsapp.presentation.adapter.AdapterLayoutMode
import id.rifqipadisiliwangi.newsapp.presentation.adapter.ViewHolderBinder

class AdapterFeedbackItem(
    var adapterLayoutMode: AdapterLayoutMode,
    private val onClickListener: (FeedbackKey) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val dataDiffer = AsyncListDiffer(
        this,
        object : DiffUtil.ItemCallback<FeedbackKey>() {
            override fun areItemsTheSame(oldItem: FeedbackKey, newItem: FeedbackKey): Boolean {
                return oldItem.id == newItem.id && oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: FeedbackKey, newItem: FeedbackKey): Boolean {
                return oldItem.hashCode() == newItem.hashCode()
            }
        }
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            AdapterLayoutMode.GRID.ordinal -> {
                GridMenuItemViewHolder(
                    binding = ItemGridFeedbackBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ),
                    onClickListener
                )
            }
            else -> {
                LinearMenuItemViewHolder(
                    binding = ItemListFeedbackBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ),
                    onClickListener
                )
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolderBinder<FeedbackKey>).bind(dataDiffer.currentList[position])
    }

    override fun getItemCount(): Int = dataDiffer.currentList.size

    override fun getItemViewType(position: Int): Int {
        return adapterLayoutMode.ordinal
    }

    fun setData(data: List<FeedbackKey>) {
        dataDiffer.submitList(data)
        notifyDataSetChanged()
    }

    fun refreshList() {
        notifyItemRangeChanged(0, dataDiffer.currentList.size)
    }

}