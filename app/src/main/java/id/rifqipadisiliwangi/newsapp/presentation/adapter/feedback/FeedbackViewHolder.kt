package id.rifqipadisiliwangi.newsapp.presentation.adapter.feedback

import androidx.recyclerview.widget.RecyclerView
import coil.load
import id.rifqipadisiliwangi.core.data.local.database.feedback.FeedbackKey
import id.rifqipadisiliwangi.newsapp.R
import id.rifqipadisiliwangi.newsapp.databinding.ItemGridFeedbackBinding
import id.rifqipadisiliwangi.newsapp.databinding.ItemListFeedbackBinding
import id.rifqipadisiliwangi.newsapp.presentation.adapter.ViewHolderBinder

class LinearMenuItemViewHolder(
    private val binding: ItemListFeedbackBinding,
    private val onClickListener: (FeedbackKey) -> Unit
) : RecyclerView.ViewHolder(binding.root), ViewHolderBinder<FeedbackKey> {


    override fun bind(item: FeedbackKey) {
        with(binding) {
            ivFeedback.load(R.drawable.ic_logo)
            ratingApp.rating = item.title.toFloat()
            tvDatetime.text = item.releaseDate
            tvContent.text = item.message
        }
        binding.root.setOnClickListener {
            onClickListener(item)
        }
    }
}

class GridMenuItemViewHolder(
    private val binding: ItemGridFeedbackBinding,
    private val onClickListener: (FeedbackKey) -> Unit
) : RecyclerView.ViewHolder(binding.root), ViewHolderBinder<FeedbackKey> {

    override fun bind(item: FeedbackKey) {
        with(binding) {
            ratingApp.rating = item.title.toFloat()
            tvDatetime.text = item.releaseDate
            tvContent.text = item.message
        }
        binding.root.setOnClickListener {
            onClickListener(item)
        }
    }
}