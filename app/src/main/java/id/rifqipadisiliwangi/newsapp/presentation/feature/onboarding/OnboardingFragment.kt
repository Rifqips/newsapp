package id.rifqipadisiliwangi.newsapp.presentation.feature.onboarding


import androidx.core.view.isGone
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.core.common.utils.Constant.onboarding_one
import id.rifqipadisiliwangi.core.common.utils.Constant.onboarding_three
import id.rifqipadisiliwangi.core.common.utils.Constant.onboarding_two
import id.rifqipadisiliwangi.core.domain.model.onboarding.OnboardingDataItem
import id.rifqipadisiliwangi.newsapp.R
import id.rifqipadisiliwangi.newsapp.databinding.FragmentOnboardingBinding
import id.rifqipadisiliwangi.newsapp.presentation.adapter.onboarding.OnboardingAdapter
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.androidx.viewmodel.ext.android.viewModel

class OnboardingFragment : BaseFragment<FragmentOnboardingBinding, ApplicationVM>(FragmentOnboardingBinding::inflate){

    override val viewModel: ApplicationVM by viewModel()

    private lateinit var vpOnboarding: ViewPager2
    private lateinit var adapter: OnboardingAdapter
    private lateinit var imageList: ArrayList<OnboardingDataItem>
    private lateinit var tabs: TabLayout

    override fun initView() {
        initListener()
        setUpvp()
    }

    private fun initListener(){
        with(binding){
            tvNext.setOnClickListener {
                val nextIndex = vpOnboarding.currentItem + 1
                vpOnboarding.setCurrentItem(nextIndex, true)
            }
            btnAuthentication.setOnClickListener {
                findNavController().navigate(R.id.action_onboardingFragment_to_registerFragment)
            }
            btnAuthentication.text = getString(R.string.string_join_now)
            tvSkipOnboarding.text = getString(R.string.string_skip)
            tvNext.text = getString(R.string.string_next)
        }
    }

    private fun setUpvp() {
        vpOnboarding = binding.vpOnboarding
        tabs = binding.tabs

        imageList = arrayListOf(
            OnboardingDataItem(onboarding_one, resources.getString(R.string.title_onboarding_one),resources.getString(R.string.description_onboarding_one)),
            OnboardingDataItem(onboarding_two,resources.getString(R.string.title_onboarding_two),resources.getString(R.string.description_onboarding_two)),
            OnboardingDataItem(onboarding_three,resources.getString(R.string.title_onboarding_three),resources.getString(R.string.description_onboarding_threee)),
        )
        adapter = OnboardingAdapter(imageList)
        vpOnboarding.adapter = adapter

        TabLayoutMediator(
            tabs,
            vpOnboarding
        ) { tab, _ ->
            tab.customView = layoutInflater.inflate(R.layout.custom_tab_layout, null)
        }.attach()

        vpOnboarding.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when (position) {
                    2 -> binding.tvNext.isGone = true
                    else -> binding.tvNext.isGone = false
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.vpOnboarding.unregisterOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {})
    }
}
