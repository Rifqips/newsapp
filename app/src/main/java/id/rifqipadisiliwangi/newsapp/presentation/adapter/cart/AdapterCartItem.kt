package id.rifqipadisiliwangi.newsapp.presentation.adapter.cart

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import id.rifqipadisiliwangi.core.common.helper.Helper.toFormattedDateTimeString
import id.rifqipadisiliwangi.core.data.local.database.cart.CartKey
import id.rifqipadisiliwangi.newsapp.databinding.ItemGridNewsBinding
import id.rifqipadisiliwangi.newsapp.presentation.adapter.ViewHolderBinder

class AdapterCartItem(
    private val onClickListener: (CartKey) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val dataDiffer = AsyncListDiffer(
        this,
        object : DiffUtil.ItemCallback<CartKey>() {
            override fun areItemsTheSame(
                oldItem: CartKey,
                newItem: CartKey
            ): Boolean {
                return oldItem.author == newItem.author
            }

            override fun areContentsTheSame(
                oldItem: CartKey,
                newItem: CartKey
            ): Boolean {
                return oldItem.hashCode() == newItem.hashCode()
            }
        }
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return GridMenuItemViewHolder(
            binding = ItemGridNewsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            onClickListener
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolderBinder<CartKey>).bind(dataDiffer.currentList[position])
    }

    override fun getItemCount(): Int = minOf(5, dataDiffer.currentList.size)

    fun setData(data: List<CartKey>) {
        dataDiffer.submitList(data.take(5))
    }

    fun refreshList() {
        notifyItemRangeChanged(0, dataDiffer.currentList.size)
    }

    class GridMenuItemViewHolder(
        private val binding: ItemGridNewsBinding,
        private val onClickListener: (CartKey) -> Unit
    ) : RecyclerView.ViewHolder(binding.root), ViewHolderBinder<CartKey> {
        override fun bind(item: CartKey) {
            with(binding) {
                val dateTimeString = item.publishedAt
                val formattedDateTime = dateTimeString?.toFormattedDateTimeString("yyyy-MM-dd HH:mm:ss")
                ivProduct.load(item.urlToImage)
                tvTitleProduct.text = item.title
                tvDatetime.text = formattedDateTime
                tvContent.text = item.content
            }
            binding.root.setOnClickListener {
                onClickListener(item)
            }
        }
    }
}
