package id.rifqipadisiliwangi.newsapp.presentation.feature.newstoday

import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.core.common.state.onLoading
import id.rifqipadisiliwangi.core.common.state.onSuccess
import id.rifqipadisiliwangi.core.common.utils.launchAndCollectIn
import id.rifqipadisiliwangi.newsapp.databinding.FragmentNewsTodayBinding
import id.rifqipadisiliwangi.newsapp.presentation.adapter.newstoday.AdapterNewsToday
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewsTodayFragment : BaseFragment<FragmentNewsTodayBinding, ApplicationVM>(
    FragmentNewsTodayBinding::inflate){

    override val viewModel: ApplicationVM by viewModel()

    private val adapterNewsToday: AdapterNewsToday by lazy {
        AdapterNewsToday{}
    }
    override fun initView() {
        initListener()
        observeData()
        with(binding){
            rvNewsToday.postDelayed({ rvNewsToday.smoothScrollToPosition(0) }, 1000)
            rvNewsToday.apply {
                layoutManager = GridLayoutManager(context,2)
                adapter = adapterNewsToday
            }
        }
    }

    private fun initListener(){
        with(binding){
            ivBack.setOnClickListener {
                findNavController().navigateUp()
            }
        }
    }

    private fun observeData(){
        viewModel.bitcoinNews.launchAndCollectIn(viewLifecycleOwner){ state->
            state.onLoading {}
                .onSuccess {
                    adapterNewsToday.setData(it.articles.toList())
                }
        }
    }

}