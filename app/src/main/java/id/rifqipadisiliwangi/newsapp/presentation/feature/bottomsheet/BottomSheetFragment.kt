package id.rifqipadisiliwangi.newsapp.presentation.feature.bottomsheet

import android.os.Bundle
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.rifqipadisiliwangi.newsapp.databinding.FragmentBottomSheetBinding

class BottomSheetFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentBottomSheetBinding

    private var filterListener: OnFilterListener? = null

    interface OnFilterListener {
        fun onFilterApplied(
            comment: String = "",
            rating: String = "",
        )
    }

    fun setFilterListener(listener: OnFilterListener) {
        filterListener = listener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBottomSheetBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        with(binding) {
            binding.etFeedback.doOnTextChanged { text, _, _, _ ->
                if (text != null) {
                    if (text.length > 1) {
                        btnFeedback.isClickable = true
                        btnFeedback.setOnClickListener {
                            val comment = binding.etFeedback.text.toString()
                            val rating = binding.ratingApp.rating.toString()
                            filterListener?.onFilterApplied(comment, rating)
                            dismiss()
                        }
                    } else {
                        btnFeedback.isClickable = false
                    }
                }
            }
        }
    }
}