package id.rifqipadisiliwangi.newsapp.presentation.feature.register

import android.Manifest
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.navigation.fragment.findNavController
import coil.load
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.core.common.helper.Helper.emailPattern
import id.rifqipadisiliwangi.core.common.helper.Helper.passwordPattern
import id.rifqipadisiliwangi.core.common.helper.Helper.toSpannableHyperLinkRegister
import id.rifqipadisiliwangi.core.common.helper.TextWatcherConfiguration
import id.rifqipadisiliwangi.core.common.state.onLoading
import id.rifqipadisiliwangi.core.common.state.onSuccess
import id.rifqipadisiliwangi.core.common.utils.Constant.IMAGE_PARSE
import id.rifqipadisiliwangi.core.common.utils.launchAndCollectIn
import id.rifqipadisiliwangi.core.data.local.database.prelogin.PreLoginKey
import id.rifqipadisiliwangi.newsapp.R
import id.rifqipadisiliwangi.newsapp.databinding.FragmentRegisterBinding
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterFragment : BaseFragment<FragmentRegisterBinding, ApplicationVM>(FragmentRegisterBinding::inflate){

    override val viewModel: ApplicationVM by viewModel()

    private var uriString : String = ""
    private lateinit var user : PreLoginKey
    lateinit var filePath: Uri

    override fun initView() {
        user = PreLoginKey()
        getBundle()
        initListener()
        setUpSpannable()
        with(binding){
            ivUser.load(R.drawable.ic_person)
            tvRegister.text = resources.getString(R.string.string_sign_up)
            etUsernameInput.hint = resources.getString(R.string.string_username)
            etEmailInput.hint = resources.getString(R.string.string_email)
            etPasswordInput.hint = resources.getString(R.string.string_password)
            btnRegister.text = resources.getString(R.string.string_creat_account)
            if (uriString.isNotEmpty()){
                val imageUri = Uri.parse(uriString)
                filePath = imageUri
                ivUser.load(imageUri)
            }
        }
    }

    private fun getBundle(){
        arguments?.let {
            it.getString(IMAGE_PARSE)?.let { uri -> uriString = uri }
        }
    }

    private fun initListener(){
        with(binding){
            ivUser.setOnClickListener {
                findNavController().navigate(R.id.action_registerFragment_to_cameraFragment)
                ActivityCompat.requestPermissions(
                    requireActivity(), arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                    ),
                    REQUEST_CODE_GALLERY
                )
            }
            if (uriString != ""){
                btnRegister.isClickable = true
                btnRegister.setOnClickListener {
                    doRegister()
                    val username = etUsernameEditLogin.text.toString()
                    val path: Uri = Uri.parse(uriString)
                    viewModel.uploadImage(path)
                    viewModel.addUsername(username)
                    if (uriString == ""){
                        Toast.makeText(context, "Picked Image", Toast.LENGTH_SHORT).show()
                    }
                }
            } else{
                btnRegister.isClickable = false
            }
            etEmailEditLogin.addTextChangedListener(TextWatcherConfiguration(5) { email ->
                validateEmail(email)
            })
            etPasswordEditLogin.addTextChangedListener(TextWatcherConfiguration(5) { password ->
                validatePassword(password)
            })
        }
    }
    private fun doRegister() {
        val email = binding.etEmailEditLogin.text.toString()
        val password = binding.etPasswordEditLogin.text.toString()
        viewModel.register(email, password)
        viewModel.registerUser.launchAndCollectIn(viewLifecycleOwner){ state->
            state.onLoading {
                binding.pbLoading.isGone = false
            }
                .onSuccess {
                    when(it){
                        true ->{
                            binding.pbLoading.isGone = true
                            Toast.makeText(context, resources.getString(R.string.string_success_register), Toast.LENGTH_SHORT).show()
                            findNavController().navigate(R.id.action_registerFragment_to_homeFragment)
                        }
                        false -> Toast.makeText(context, resources.getString(R.string.string_failed_register), Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }

    private fun validateEmail(email: String) : Boolean {
        return if(email.isEmpty() || emailPattern.matcher(email).matches() || email.length <= 1 ){
            with(binding){
                etEmailInput.isErrorEnabled = false
                etEmailInput.isHelperTextEnabled = true
                etEmailInput.helperText = resources.getString(R.string.string_example_gmail_com)
            }
            false
        } else{
            with(binding){
                etEmailInput.error = resources.getString(R.string.string_email_is_not_valid)
                etEmailInput.isHelperTextEnabled = false
            }
            true
        }
    }
    private fun validatePassword(password: String) : Boolean {
        with(binding){
            return if (passwordPattern.matcher(password).matches() ||password.length <= 5) {
                etPasswordInput.isErrorEnabled = false
                etPasswordInput.isHelperTextEnabled = true
                etPasswordInput.helperText = getString(R.string.string_helper_password)
                true
            } else {
                etPasswordInput.error = getString(R.string.string_helper_password)
                etPasswordInput.isHelperTextEnabled = false
                false
            }
        }
    }

    private fun setUpSpannable(){
        with(binding){
            tvLogin.movementMethod = LinkMovementMethod.getInstance()
            val color = context?.let { ContextCompat.getColor(it, R.color.green) }

            val actionRegister : () -> Unit = {
                findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
            }

            if (color != null){
                tvLogin.text = getString(R.string.string_do_you_have_account_sign_in)
                    .toSpannableHyperLinkRegister(
                        resources.configuration.locales[0].language,
                        color,
                        actionRegister,
                    )
            }
        }

    }
    private fun analyticsFirebase(message : String, bundle : Bundle){
        viewModel.logEvent(message, bundle)
        viewModel.debugSreenView(message)
    }

    override fun onResume() {
        super.onResume()
        val message = resources.getString(R.string.string_sign_up)
        val bundle = Bundle().apply { putString(resources.getString(R.string.string_sign_up), message) }
        analyticsFirebase(message, bundle)
    }

    companion object{
        private val REQUEST_CODE_GALLERY = 999
    }

}