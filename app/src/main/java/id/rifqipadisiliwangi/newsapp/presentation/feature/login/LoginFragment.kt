package id.rifqipadisiliwangi.newsapp.presentation.feature.login

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.navigation.fragment.findNavController
import id.rifqipadisiliwangi.core.common.base.BaseFragment
import id.rifqipadisiliwangi.core.common.helper.Helper
import id.rifqipadisiliwangi.core.common.helper.Helper.toSpannableHyperLink
import id.rifqipadisiliwangi.core.common.helper.TextWatcherConfiguration
import id.rifqipadisiliwangi.core.common.state.onError
import id.rifqipadisiliwangi.core.common.state.onLoading
import id.rifqipadisiliwangi.core.common.state.onSuccess
import id.rifqipadisiliwangi.core.common.utils.launchAndCollectIn
import id.rifqipadisiliwangi.newsapp.R
import id.rifqipadisiliwangi.newsapp.databinding.FragmentLoginBinding
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : BaseFragment<FragmentLoginBinding, ApplicationVM>(FragmentLoginBinding::inflate){

    override val viewModel: ApplicationVM by viewModel()

    override fun initView() {
        initListener()
        setUpSpannable()
        with(binding){
            tvLogin.text = resources.getString(R.string.string_sign_in)
            etEmailInput.hint = resources.getString(R.string.string_email)
            etPasswordInput.hint = resources.getString(R.string.string_password)
            btnLogin.text = resources.getString(R.string.string_log_in)
        }
    }

    private fun initListener(){
        with(binding){
            btnLogin.setOnClickListener {
                doLogin()
            }
            etEmailEditLogin.addTextChangedListener(TextWatcherConfiguration(5) { email ->
                validateEmail(email)
            })
            etPasswordEditLogin.addTextChangedListener(TextWatcherConfiguration(5) { password ->
                validatePassword(password)
            })
        }
    }
    private fun doLogin() {
        val email = binding.etEmailEditLogin.text.toString()
        val password = binding.etPasswordEditLogin.text.toString()
        viewModel.login(email, password)
        viewModel.loginUser.launchAndCollectIn(viewLifecycleOwner){ state->
            state.onLoading {
                binding.pbLoading.isGone = false

            }
                .onSuccess {
                    binding.pbLoading.isGone = true
                    when(it){
                        true -> {
                            findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
                            Toast.makeText(context, "Sukses Login", Toast.LENGTH_SHORT).show()
                        }
                        else -> Toast.makeText(context, resources.getString(R.string.string_failed_login), Toast.LENGTH_SHORT).show()
                    }
                }
                .onError {
                    Log.d("loginFragment", "onError")
                }
        }
    }

    private fun validateEmail(email: String) : Boolean {
        return if(email.isEmpty() || Helper.emailPattern.matcher(email).matches() || email.length <= 5 ){
            with(binding){
                etEmailInput.isErrorEnabled = false
                etEmailInput.isHelperTextEnabled = true
                etEmailInput.helperText = resources.getString(R.string.string_example_gmail_com)
                btnLogin.isClickable = true
            }
            false
        } else{
            with(binding){
                etEmailInput.error = resources.getString(R.string.string_email_is_not_valid)
                etEmailInput.isHelperTextEnabled = false
                btnLogin.isClickable = false
            }
            true
        }
    }
    private fun validatePassword(password: String) : Boolean {
        with(binding){
            return if (Helper.passwordPattern.matcher(password).matches() ||password.length <= 1) {
                etPasswordInput.isErrorEnabled = false
                etPasswordInput.isHelperTextEnabled = true
                etPasswordInput.helperText = getString(R.string.string_helper_password)
                btnLogin.isClickable = true
                true
            } else {
                etPasswordInput.error = getString(R.string.string_helper_password)
                etPasswordInput.isHelperTextEnabled = false
                btnLogin.isClickable = false
                false
            }
        }
    }

    private fun setUpSpannable(){
        with(binding){
            tvRegister.movementMethod = LinkMovementMethod.getInstance()
            val color = context?.let { ContextCompat.getColor(it, R.color.green) }

            val actionRegister : () -> Unit = {
                findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            }

            if (color != null){
                tvRegister.text = getString(R.string.string_dont_have_account)
                    .toSpannableHyperLink(
                        resources.configuration.locales[0].language,
                        color,
                        actionRegister,
                    )
            }
        }
    }
    private fun analyticsFirebase(message: String, bundle: Bundle) {
        viewModel.logEvent(message, bundle)
        viewModel.debugSreenView(message)
    }

    override fun onResume() {
        super.onResume()
        val message = resources.getString(R.string.string_sign_in)
        val bundle =
            Bundle().apply { putString(resources.getString(R.string.string_sign_in), message) }
        analyticsFirebase(message, bundle)
    }
}