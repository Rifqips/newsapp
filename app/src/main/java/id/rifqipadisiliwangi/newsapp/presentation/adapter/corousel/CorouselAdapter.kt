package id.rifqipadisiliwangi.newsapp.presentation.adapter.corousel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import id.rifqipadisiliwangi.core.domain.model.onboarding.OnboardingDataItem
import id.rifqipadisiliwangi.newsapp.databinding.ItemContainerCorouselBinding

class CorouselAdapter(
    private val imageList: ArrayList<OnboardingDataItem>,
) : RecyclerView.Adapter<CorouselAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemContainerCorouselBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = imageList[position]
        holder.bind(currentItem)
    }

    override fun getItemCount(): Int {
        return imageList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding: ItemContainerCorouselBinding? = ItemContainerCorouselBinding.bind(itemView)

        fun bind(data: OnboardingDataItem) {
            binding?.apply {
                tvItem.text = data.title
                ivItem.load(data.image)
            }
        }
    }
}