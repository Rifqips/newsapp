package id.rifqipadisiliwangi.newsapp.di

import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.auth.ktx.auth
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import id.rifqipadisiliwangi.core.common.helper.PreferenceDataStoreHelper
import id.rifqipadisiliwangi.core.common.helper.PreferenceDataStoreHelperImpl
import id.rifqipadisiliwangi.core.data.datasource.AppPreferencesDataSource
import id.rifqipadisiliwangi.core.data.datasource.AppPreferencesDataSourceImpl
import id.rifqipadisiliwangi.core.data.datasource.NewsDataSource
import id.rifqipadisiliwangi.core.data.datasource.NewsDataSourceImpl
import id.rifqipadisiliwangi.core.data.local.database.ApplicationDatabase
import id.rifqipadisiliwangi.core.data.local.datastore.appDataSource
import id.rifqipadisiliwangi.core.data.remote.client.NetworkClient
import id.rifqipadisiliwangi.core.data.remote.interceptor.ApplicationInterceptor
import id.rifqipadisiliwangi.core.data.remote.service.ApiEndpoint
import id.rifqipadisiliwangi.core.domain.repository.database.AppRoomRepository
import id.rifqipadisiliwangi.core.domain.repository.database.AppRoomRepositoryImpl
import id.rifqipadisiliwangi.core.domain.repository.firebase.FirebaseRepository
import id.rifqipadisiliwangi.core.domain.repository.firebase.FirebaseRepositoryImpl
import id.rifqipadisiliwangi.core.domain.repository.news.NewsRepositoryImpl
import id.rifqipadisiliwangi.core.domain.usecase.news.BitcoinUseCase
import id.rifqipadisiliwangi.core.domain.usecase.news.NewsUseCase
import id.rifqipadisiliwangi.core.domain.usecase.news.SearchUseCase
import id.rifqipadisiliwangi.core.domain.usecase.prelogin.GetUserUseCase
import id.rifqipadisiliwangi.core.domain.usecase.prelogin.LoginUseCase
import id.rifqipadisiliwangi.core.domain.usecase.prelogin.RegisterUseCase
import id.rifqipadisiliwangi.newsapp.presentation.viewmodel.ApplicationVM
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.Module
import org.koin.dsl.module

object AppModules {

    private val viewModelModule = module {
        viewModelOf(::ApplicationVM)
    }

    private val utils = module {
        single<PreferenceDataStoreHelper> { PreferenceDataStoreHelperImpl(get()) }
    }

    private val firebaseModule = module{
        single { Firebase.analytics }
        single { Firebase.crashlytics }
        single { Firebase.database }
        single { Firebase.auth }
        single { Firebase.storage.reference }
    }
    private val networkModules = module{
        single { ApplicationInterceptor() }
        single { ChuckerInterceptor.Builder(androidContext()).build() }
        single { NetworkClient(get(), get()) }
        single<ApiEndpoint>{get<NetworkClient>().create()}
    }

    private val sourceModule = module{
        single<AppPreferencesDataSource> { AppPreferencesDataSourceImpl(get()) }
        single<NewsDataSource> { NewsDataSourceImpl(get()) }
    }

    private val repositoryModule = module{
        single <FirebaseRepository>{ FirebaseRepositoryImpl(get(), get()) }
        single <AppRoomRepository>{ AppRoomRepositoryImpl(get(), get(), get()) }
        single { NewsRepositoryImpl(get()) }
    }

    private val usecaseModule = module{
        single { LoginUseCase(get()) }
        single { RegisterUseCase(get()) }
        single { GetUserUseCase(get()) }
        single { NewsUseCase(get()) }
        single { BitcoinUseCase(get()) }
        single { SearchUseCase(get()) }
    }

    private val localModule = module {
        single { androidContext().appDataSource }
        single { ApplicationDatabase.getInstance(get()) }
        single { get<ApplicationDatabase>().preLoginDao() }
        single { get<ApplicationDatabase>().feedbackDao() }
        single { get<ApplicationDatabase>().cartDao() }
    }

    val modules: List<Module> = listOf(
        viewModelModule,
        networkModules,
        sourceModule,
        repositoryModule,
        usecaseModule,
        localModule,
        firebaseModule,
        utils
    )
}